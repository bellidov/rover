/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package myproject;

/**
 *
 * @author Xrup
 */
public class LoginCommand implements RoverCommand {
    private RoverCommand command;
    
    public LoginCommand(RoverCommand command){
        this.command = command;
    }
    
    @Override
    public void execute() {
        command.execute();
        System.out.println("Log: was used command" + command.toString());
    }
    
}
