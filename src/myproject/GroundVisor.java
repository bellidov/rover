/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package myproject;

/**
 *
 * @author Xrup
 */
public class GroundVisor implements Visor {
    private Ground ground;
    
    public GroundVisor(){
        
    }
    
    public GroundVisor(Ground ground){
        this.ground = ground;
    }
    
    public boolean hasObstacles(int x, int y){
        if(x >= ground.getWidth() || y >= ground.getHeigth()){
            throw new GroundVisorException("This point is too far for me...");
        }
   //     return ground.getCell(x, y).getState().equals(CellState.FREE) ? false : true;
        return !ground.getCell(x, y).getState().equals(CellState.FREE);
    }

    public void setGround(Ground ground) {
        this.ground = ground;
    }
}
