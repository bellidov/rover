/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package myproject;

/**
 *
 * @author Xrup
 */
public class TurnCommand implements RoverCommand
{

    private Turnable turnable;
    private Direction dir;
    public TurnCommand(Turnable turnable, Direction dir){
        this.turnable = turnable;
        this.dir = dir;
    }
    @Override
    public void execute() {
        turnable.turnTo(dir);
    }
    
}
