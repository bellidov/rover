package myproject;

import org.junit.Test;
import static org.junit.Assert.*;

public class GroundTest {
    
    public GroundTest() {
    }

    @Test
    public void testCreateGround() {
        Ground ground = new Ground(1000, 500);
        
        GroundCell cell = ground.getCell(999, 499);
        ground.createGround();
        assertNotNull(cell);
    }

    @Test
    public void testGetHeigth() {
        Ground g = new Ground(100, 50);
        
        int expected = 50;
        int heigth = g.getHeigth();
        
        assertEquals("heigth", expected, heigth);
    }

    @Test
    public void testGetWidth() {
        Ground g = new Ground(100, 50);
        
        int expected = 100;
        int width = g.getWidth();
        
        assertEquals("Width", expected, width);
    }

    @Test
    public void testGetCell() {
       Ground g = new Ground(100, 50);
       
       GroundCell cell = g.getCell(1, 2);
       GroundCell expected = new GroundCell(1, 2, CellState.FREE);
       
        assertTrue("Objects no equals, ", expected.equals(cell));
    }
    
}
