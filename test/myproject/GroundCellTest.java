package myproject;

import org.junit.Test;
import static org.junit.Assert.*;

public class GroundCellTest {
    
    public GroundCellTest() {
    }
    
    @Test
    public void testGetState() {
        System.out.println("getState");
        GroundCell cell = new GroundCell(1, 2, CellState.OCCUPIED);
        CellState expected = CellState.OCCUPIED;
        CellState result = cell.getState();
        assertEquals("State", expected, result);
    }
    
}
