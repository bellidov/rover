/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package myproject;

/**
 *
 * @author Xrup
 */
public class MoveCommand implements RoverCommand{

    private Moveable moveable;
    private int x, y;
    public MoveCommand(Moveable moveable, int x, int y){
        this.moveable = moveable;
        this.x = x;
        this.y = y;
    }
    
    @Override
    public void execute() {
        moveable.moveTo(x, y);
    }
    
}
