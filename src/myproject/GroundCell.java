/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package myproject;

/**
 *
 * @author Xrup
 */
public class GroundCell {
    private CellState state;
    private int x;
    private int y;
    
    public GroundCell(int x, int y, CellState state){
        this.x = x;
        this.y = y;
        this.state = state;
    }
    
    public CellState getState(){
        return this.state;
    }
    
    @Override
    public boolean equals(Object cell){
        if (cell==this) return true;
        if (!(cell instanceof GroundCell)) return false;
        
         GroundCell gcell = (GroundCell)cell;
         return gcell.state == state && gcell.x == x && gcell.y == y;
    }
 }
