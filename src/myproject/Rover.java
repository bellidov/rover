/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package myproject;

/**
 *
 * @author Xrup
 */
public class Rover implements Turnable, Moveable, ProgramFileAware //IMovable, ITurnable
{
    private int x, y;
    private Direction dir;
    private GroundVisor visor;
    private RoverCommandParser programParser;
    
    public Rover(){
        System.out.println("Rover is ready!");
        visor = new GroundVisor();
        
    }
    
    @Override
    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
        
        if(!visor.hasObstacles(x, y)){
            System.out.println("I move to x = " + x + ", y = " + y);
        }
        else{
            System.out.println("this point is occupied...");
        }
        
    }

    @Override
    public void turnTo(Direction dir) {
        this.dir = dir;
        System.out.println("I turn to " + dir);
    }

    public GroundVisor getVisor() {
        
        return this.visor;
    }

    @Override
    public void executeProgramFile(String s) {
        programParser = new RoverCommandParser(this, s);
        RoverCommand command;
        while((command = programParser.readNextCommand()) != null){
            new LoginCommand(command).execute();
        }
    }      
}