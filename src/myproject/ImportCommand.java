/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package myproject;

import java.util.List;

/**
 *
 * @author Xrup
 */
public class ImportCommand implements RoverCommand{

    private List<RoverCommand> commands;
    
    public ImportCommand(List<RoverCommand> commands){
        this.commands = commands;
    }
    
    @Override
    public void execute() {
        for(RoverCommand c : commands){
            c.execute();
        }
    }
    
}
