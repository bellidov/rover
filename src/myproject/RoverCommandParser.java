/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package myproject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Xrup
 */
public class RoverCommandParser {
    
    private Rover rover;
    private BufferedReader reader;
    private org.w3c.dom.Document xmlReader;
    private String file;

    public RoverCommandParser(Rover rover, String file){
        this.rover = rover;
        this.file = file;
    }
    
    private BufferedReader getReader() {
        if (reader == null){
            try {
                reader = new BufferedReader(new FileReader(new File(file)));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(RoverCommandParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return reader;
    }

    private org.w3c.dom.Document getReaderXML(){
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            xmlReader = db.parse(file);
        } catch (Exception ex) {
      //      Logger.getLogger(RoverCommandParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return xmlReader;
    }
    
    private int count = 0;
    public RoverCommand readNextCommand(){
        
        if(file.endsWith(".xml")){
            xmlReader = getReaderXML();
            Element dom = xmlReader.getDocumentElement();
            NodeList commands = dom.getElementsByTagName("commands");
            RoverCommand command = null;
            
            if(commands.item(count).getNodeName().equals("turn")){
                command = new TurnCommand(rover, Direction.valueOf(commands.item(count).getChildNodes().item(0).getNodeName()));
            }
            else if(commands.item(count).getNodeName().equals("move")){
                command = new MoveCommand(rover, Integer.parseInt((commands.item(count).getChildNodes().item(0).getChildNodes().item(0).getNodeName())), 
                                                 Integer.parseInt((commands.item(count).getChildNodes().item(0).getChildNodes().item(1).getNodeName())));
            }
            
            count++;
            return command;
        }
        
        RoverCommand command = null;
        String com = null;
        try {
            com = getReader().readLine();
        } catch (IOException ex) {
            Logger.getLogger(RoverCommandParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (com == null){
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(RoverCommandParser.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
        
        String[] words = com.split(" ");
        
        switch(words[0]){
            case "move":
                command = new MoveCommand(rover, Integer.parseInt(words[1]), Integer.parseInt(words[2]));
                break;
            case "turn":
                command = new TurnCommand(rover, Direction.valueOf(words[1].toUpperCase()));
                break;
            case "import":
                List<RoverCommand> newCommands = new LinkedList();
                RoverCommand newCommand;
                RoverCommandParser newParser = new RoverCommandParser(rover, words[1]);
                while((newCommand = newParser.readNextCommand()) != null){
                    newCommands.add(newCommand);
                }
                command = new ImportCommand(newCommands);
                break;
        }
        return command;
    }
    
}
