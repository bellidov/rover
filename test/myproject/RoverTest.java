package myproject;

import org.junit.Test;
import static org.junit.Assert.*;

public class RoverTest {
    
    public RoverTest() {
    }

    @Test
    public void testGetVisor() {
        Rover rover = new Rover();
        GroundVisor visor = rover.getVisor();
        assertNotNull("visor is null", visor);
    }
    
}
