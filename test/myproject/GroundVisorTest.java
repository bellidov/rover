package myproject;

import org.junit.Test;
import static org.junit.Assert.*;

public class GroundVisorTest {
    
    public GroundVisorTest() {
    }

    @Test
    public void testHasObstacles() {
        Ground ground = new Ground(200, 100);
        GroundVisor visor = new GroundVisor(ground); 
        
        boolean result = visor.hasObstacles(1, 2);
        boolean expected = false;
        
        assertTrue("Error hasObstacles visor", expected == result);
    }

    @Test
    public void testSetGround() {
        Ground ground1 = new Ground(200, 100);
        Ground ground2 = new Ground(1000, 500);
        
        GroundVisor visor = new GroundVisor(ground1);
        visor.setGround(ground2);
        
        boolean result = visor.hasObstacles(500, 200);
        boolean expected = false;
        
        assertTrue("Set ground in visor error", result == expected);
    }
}
