/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package myproject;

import java.awt.Graphics;

/**
 *
 * @author Xrup
 */
public class Ground {
    private int heigth;
    private int width;
    private GroundCell[][] landscape;
    
    public Ground(int width, int heigth){
        this.heigth = heigth;
        this.width = width;
        createGround();
    }
    
    protected void createGround(){
        landscape = new GroundCell[width][heigth];
        for(int i = 0; i < width; i++){
            for(int j = 0; j < heigth; j++){
                landscape[i][j] = new GroundCell(i, j, CellState.FREE);
            }
        }
    }
    
    public int getHeigth(){
        return this.heigth;
    }
    
    public int getWidth(){
        return this.width;
    }
    
    public GroundCell getCell(int x, int y){
        return landscape[x][y];
    }
}
